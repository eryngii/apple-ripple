const gulp = require('gulp')
const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const server = require('browser-sync').create()
const pug = require('gulp-pug')
const stylus = require('gulp-stylus')
const nib = require('nib')
const webpack = require('webpack')
const webpackStream = require('webpack-stream')
const webpackConfig = require('./webpack.config')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const paths = {
  pug: {
    watch: ['src/pug/**/*.pug'],
    base: 'src/pug/',
    src: ['./src/pug/**/*.pug', '!src/pug/_*.pug'],
    dist: 'dist/'
  },
  stylus: {
    watch: ['src/stylus/**/*.styl'],
    base: 'src/stylus/',
    src: ['src/stylus/**/*.styl', '!src/stylus/**/_*.styl', '!src/stylus/**/_*/**/*'],
    dist: 'dist/'
  },
  js: {
    watch: ['src/js/**/*'],
    src: [''],
    dist: 'dist/js/'
  }
}
const plumberOption = {
  errorHandler: notify.onError('<%= error.message =%>')
}

const serve = done => {
  const bundler = webpack(webpackConfig)
  server.init({
    notify: false,
    // proxy: 'http://localhost:8888',
    server: { baseDir: 'dist/' },
    middleware: [
      webpackDevMiddleware(bundler, {
        quiet: true,
        path: webpackConfig.output.path,
        publicPath: webpackConfig.output.publicPath,
        index: 'index.html',
        stats: {
          color: true
        }
      }),
      webpackHotMiddleware(bundler)
    ]
  })
  done()
}

const watch = done => {
  gulp.watch(paths.pug.watch, pug2html)
  gulp.watch(paths.stylus.watch, stylus2css)
  // gulp.watch(paths.js.watch, bundleJs)
  done()
}

const pug2html = done => {
  return gulp.src(paths.pug.src, paths.pug.base)
    .pipe(plumber(plumberOption))
    .pipe(pug())
    .pipe(gulp.dest(paths.pug.dist))
    .pipe(server.stream())
}

const stylus2css = done => {
  return gulp.src(paths.stylus.src, paths.stylus.base)
    .pipe(plumber(plumberOption))
    .pipe(stylus({ use: nib() }))
    .pipe(gulp.dest(paths.stylus.dist))
    .pipe(server.stream())
}

const bundleJs = done => {
  return webpackStream(webpackConfig, webpack)
    .pipe(plumber(plumberOption))
    .pipe(gulp.dest(paths.js.dist))
    .pipe(server.stream())
}

const build = done => {
  gulp.parallel(pug2html, stylus2css, bundleJs)
  done()
}

gulp.task('build', build)
gulp.task('default', gulp.series(serve, watch))
