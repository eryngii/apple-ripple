module.exports = function(api) {
  api.cache(true)
  return {
    plugins: [
      ['@babel/plugin-proposal-pipeline-operator', { proposal: 'minimal' }],
      '@babel/plugin-proposal-class-properties',
      ['@babel/plugin-proposal-decorators', { legacy: true }]
    ],
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            browsers: ['last 2 versions']
          }
        }
      ],
      '@babel/preset-flow',
      '@babel/preset-react'
    ]
  }
}
